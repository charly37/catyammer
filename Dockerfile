FROM centos:7
MAINTAINER charles.walker.37@gmail.com

RUN yum -y update

# Install NodeJs
RUN yum -y groupinstall development
RUN yum -y install -y gcc-c++ make
RUN curl -sL https://rpm.nodesource.com/setup_12.x | bash -
RUN yum -y install nodejs

ADD https://upload.wikimedia.org/wikipedia/commons/4/4d/Cat_November_2010-1a.jpg /testpic.jpg

COPY ./myapp /prod
WORKDIR "/prod"
RUN npm install

CMD ["npm","start"]