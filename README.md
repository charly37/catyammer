# catYammer

To create a yammer application that post cat in yammer room

# Build 
```sh
docker build -t catyammer .
```
# Run (for local dev)
```sh
docker run -v C:\Code\catConnector\myapp\:/myapp -p 443:443 -it catyammer bash
```
# Run (prod)
```sh
docker run -d -v /etc/letsencrypt:/etc/letsencrypt -p 443:443 --env CLIENT_SECRET=foo --env CAT_API_KEY=bar --env DOG_API_KEY=foo --env MY_API_KEY=mysecretkey catyammer
```
- MY_API_KEY is a random string used to prevent other people to use your API
- CLIENT_SECRET is your yammer application secret
- CAT_API_KEY is your REST API token for https://thecatapi.com/
- DOG_API_KEY is your REST API token for https://www.thedogapi.com/

# Crontab (optional)
Once the server is running you can add this to your crontab to trig a post everyday
```sh
0 1 * * * curl https://djynet.xyz/postdog?room=<roomID>&key=<mysecretkey>
```