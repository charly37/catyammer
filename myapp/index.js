const express = require('express');
// To do POST call - default nodejs module
// Also use to create the server
var https = require('https')
const fs = require('fs');
const app = express();
// To do POST call - more high level than default nodejs module https
const axios = require('axios')
// To add image to post
var FormData = require('form-data');
//to extract filename from urls
const path = require('path');

/* ----------------------------------- ROUTES ---------------------------- */

// Login endpoint
app.get('/login', (req, res) => {
    var aLoginUri = "https://www.yammer.com/oauth2/authorize?client_id=CN737QnN3TCu2ooY7U2rbA&response_type=code&redirect_uri=https://djynet.xyz/callback";
    res.send(aLoginUri);
    console.log('Sent login URI response');
});

// OAuth2 endpoint (callback)
app.get('/callback', (req, res) => {
    res.end()
    console.log('Received Oauth login callback with code ', req.url);

    //Calling Oauth to authenticate the APP
    var aUriAuthent = "https://www.yammer.com/oauth2/access_token?client_id=CN737QnN3TCu2ooY7U2rbA&client_secret=" + aClientSecret + "&code=" + req.query.code + "&grant_type=authorization_code";
    axios.post(aUriAuthent)
        .then((res) => {
            //console.log("Dumping response for debuging: " +res)
            //console.log("Dumping data from response for Debug: ", res.data)
            aAUthTest2 = res.data;
            console.log("aAUthTest2: ", aAUthTest2)
        })
        .catch((error) => {
            console.error(error)
        })
});

// test post simple message - no picture attach
app.get('/postsimple', (req, res) => {
    console.log("Entering /postsimple route")
    //console.log("Download image")
    //saveImageToDisk()
    console.log("Upload image to Yammer")
    uploadToYammer("testpic.jpg").then(aImgId => {
        console.log("aImgId from uploadToYammer: ",aImgId)
        console.log('Going to post test message in room: ', req.query.room);
        postToYammer(req.query.room,aImgId)
    })
    res.send("OK");
});

// post Cat callback
app.get('/postcat', (req, res) => {
    console.log("Entering /postcat route")

    if (req.query.key !== aPostCatSecretKey) {
        console.log("Invalid key: ", req.query.key, " - send back 401")
        res.sendStatus(401);
    }
    else {
        console.log("Going to try to post on yammer room")
        const aRoomId= req.query.room;
        console.log("aRoomId: ",aRoomId)
        processCat(aRoomId)
        res.send("OK");
    }
});

// post Dog callback
app.get('/postdog', (req, res) => {
    console.log("Entering /postdog route")

    if (req.query.key !== aPostCatSecretKey) {
        console.log("Invalid key: ", req.query.key, " - send back 401")
        res.sendStatus(401);
    }
    else {
        console.log("Processing dog")
        const aRoomId= req.query.room;
        console.log("aRoomId: ",aRoomId)
        processDog(aRoomId)
        res.send("OK");
        
    }
});

// Handles any requests that don't match the ones above
app.get('*', (req, res) => {
    console.log('Unknow route: ', req.url);
    return res.status(404).send({ message: 'Route' + req.url + ' Not found.' });
});

/* ----------------------------------- PROCESS ---------------------------- */

async function processDog(aRoomId) {
    console.log('Entering processDog');
    const aOneDogUrl = await getDogUrlFromTheDogApi()
    console.log("aOneDogUrl:", aOneDogUrl)
    const aFileName = path.basename(aOneDogUrl);
    console.log("Download it localy with name: ", aFileName)
    await saveImageToDisk(aOneDogUrl,aFileName)
    console.log('Image saved. Uploading to yammer ');
    const aImageId = await uploadToYammer(aFileName);
    console.log('image uploaded. Now posting');
    postToYammer(aRoomId,aImageId)
    console.log('Existing processDog ');
    return;
}

async function processCat(aRoomId) {
    console.log('Entering processCat');
    const aOneCatUrl = await getCatUrl()
    console.log("aOneCatUrl:", aOneCatUrl)
    const aFileName = path.basename(aOneCatUrl);
    console.log("Download it localy with name: ", aFileName)
    await saveImageToDisk(aOneCatUrl,aFileName)
    console.log('Image saved. Uploading to yammer ');
    const aImageId = await uploadToYammer(aFileName);
    console.log('image uploaded. Now posting');
    postToYammer(aRoomId,aImageId)
    console.log('Existing processCat ');
    return;
}

/* ----------------------------------- MWs ---------------------------- */

async function saveImageToDisk(aUrl, localPath) {
    console.log('Entering saveImageToDisk');

  const aPath = path.resolve("/", localPath)

  // axios image download with response type "stream"
  const response = await axios({
    method: 'GET',
    url: aUrl,
    responseType: 'stream'
  })

  // pipe the result stream into a file on disc
  response.data.pipe(fs.createWriteStream(aPath))

  // return a promise and resolve when download finishes
  return new Promise((resolve, reject) => {
    response.data.on('end', () => {
      resolve()
    })

    response.data.on('error', () => {
      reject()
    })
  })
    
      console.log('Exiting saveImageToDisk');
}

function postToYammer(iRoomId,iFileId) {
    //https://developer.yammer.com/docs/upload-files-into-yammer-groups

    console.log('Entering postToYammer function with iRoomId:', iRoomId, ' and iFileId:',iFileId)

    var formData = new FormData();
    //Date time to add in msg
    var aDateTime = new Date().toISOString()
    // Fill the formData object
    formData.append('body', 'Picture of the day - '+aDateTime);
    formData.append('group_id', iRoomId);
    formData.append('attached_objects[]', 'uploaded_file:'+iFileId);


    // Patch header to add the key
    var aHeader = formData.getHeaders();
    aHeader['Authorization'] = "Bearer " + aAUthTest2.access_token.token;

    console.log('Going to post test MSG');

    aPostCall = axios.post('https://www.yammer.com//api/v1/messages.json?', formData, {
        headers: {
            ...aHeader,
        },
    })
    aRes =aPostCall.data
    //console.log('Finish posting test MSG to yammer: ', aRes); //verbose
    console.log('Exiting postToYammer function')
}

async function uploadToYammer(localPath) {
    //https://developer.yammer.com/docs/upload-files-into-yammer-groups

    console.log("Entering uploadToYammer function with localPath",localPath)

    var formData = new FormData();
    // Fill the formData object
    formData.append('network_id', '7454852');
    formData.append('target_type', 'PRIVATE_MESSAGE');
    formData.append('filename', localPath); //VERY important to have a jpg extension
    formData.append('file', fs.createReadStream("/"+localPath));

    // Patch header to add the key
    var aHeader = formData.getHeaders();
    aHeader['Authorization'] = "Bearer " + aAUthTest2.access_token.token;

    console.log('Going to post test image');

    const aPostCall = await axios.post('https://filesng.yammer.com/v4/uploadSmallFile', formData, {
        headers: {
            ...aHeader,
        },
    })
    aRes =aPostCall.data
    //console.log('Finish posting image to yammer: ', aRes); //verbose
    aImageId = aRes.id
    console.log('aImageId: ', aImageId);
    return aImageId
}

async function getCatUrl() {
    console.log('Entering getCatUrl');
    var aCatUrl = "https://upload.wikimedia.org/wikipedia/commons/4/4d/Cat_November_2010-1a.jpg";
    const aTemp = await axios.get("https://api.thecatapi.com/v1/images/search", { params: {}, headers: { 'x-api-key': aCatApiKey } });
    aCatUrl = aTemp.data[0].url
    console.log('Existing getCatUrl with: ', aCatUrl);
    return aCatUrl;
}

async function getDogUrlFromTheDogApi() {
    console.log('Entering getDogUrlFromTheDogApi');
    var aDogUrl = "https://upload.wikimedia.org/wikipedia/commons/4/4d/Cat_November_2010-1a.jpg";
    const aTemp = await axios.get("https://api.thedogapi.com/v1/images/search", { params: {}, headers: { 'x-api-key': aDogApiKey } });
    aDogUrl = aTemp.data[0].url
    console.log('Existing getDogUrl with: ', aDogUrl);
    return aDogUrl;
}

async function getDogUrlFromDogCeo() {
    console.log('Entering getDogUrlFromDogCeo');
    var aDogUrl = "https://upload.wikimedia.org/wikipedia/commons/4/4d/Cat_November_2010-1a.jpg";
    const aTemp = await axios.get("https://dog.ceo/api/breeds/image/random");
    aDogUrl = aTemp.data[0].message
    console.log('Existing getDogUrl with: ', aDogUrl);
    return aDogUrl;
}

/* ----------------------------------- MAIN ---------------------------- */

// APP secret code
const aClientSecret = process.env.CLIENT_SECRET;
console.log("Your client secret is ", aClientSecret);
const aCatApiKey = process.env.CAT_API_KEY;
console.log("Your cat api key is ", aCatApiKey);
const aDogApiKey = process.env.DOG_API_KEY;
console.log("Your dog api key is ", aDogApiKey);
const aPostCatSecretKey = process.env.MY_API_KEY;
console.log("Secret key for post cat route ", aPostCatSecretKey);

if (aClientSecret === undefined || aCatApiKey === undefined || aDogApiKey === undefined || aPostCatSecretKey === undefined) {
    console.log("Please export you client secrets");
    process.exit(1);
}

var aAUthTest2;

// Certificate
const privateKey = fs.readFileSync('/etc/letsencrypt/live/djynet.xyz/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/djynet.xyz/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/djynet.xyz/fullchain.pem', 'utf8');

const credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca
};

const httpsServer = https.createServer(credentials, app);

const port = process.env.PORT || 443;
httpsServer.listen(port, () => {
    console.log('App is listening on port ', port);
});